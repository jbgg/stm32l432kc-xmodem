#ifndef __IO_H__
#define __IO_H__

void io_printf(const char *fmt, ...);
void io_putchar(int c);
char io_getchar();

#endif /* __IO_H__ */
